const path = require('path');

module.exports =  
    {
        entry: path.join(__dirname, '/src/index.js'),
        devtool: 'inline-source-map',
        output: {
            path: path.join(__dirname, '/dist/js'),
            filename: 'main.js',
        },
        module: {
            rules: [
                { 
                    test: /\.js$/, 
                    exclude: /node_modules/, 
                    loader: 'babel-loader'
                }
            ]
        }
    };