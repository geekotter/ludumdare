import { CREATE_DECK } from "./types";

export function createDeck(name, cards) {
    return {
        type: CREATE_DECK,
        payload: {
            name,
            cards
        }
    };
}