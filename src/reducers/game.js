import { HEALTH, SPAWNING, ONGOING } from "../costants";

const initialState = {
    road: [],
    towers: [],
    baseHP: 100,
    mana: 10,
    temple: HEALTH,
    turn: 0,
    phase: SPAWNING,
    hand: [],
    status: ONGOING
};

function reducer (state = initialState, action){
    return state;
}

export default reducer;