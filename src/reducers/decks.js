import shortid from "shortid";

import { CREATE_DECK } from "../actions/types";

const initialState = [
    {
        id: shortid.generate(),
        name: "First",
        cards:[
            {
                cardId: 1,
                copies: 2
            },
            {
                cardId: 2,
                copies: 3
            },
            {
                cardId: 3,
                copies: 1
            }
        ]
    },
    {
        id: shortid.generate(),
        name: "Second",
        cards:[
            {
                cardId: 1,
                copies: 4
            },
            {
                cardId: 2,
                copies: 2
            },
            {
                cardId: 3,
                copies: 3
            }
        ]
    }
];

function reducer (state = initialState, action){
    switch (action.type) {
        case CREATE_DECK:
            const newDeck = {
                id: shortid.generate(),
                name: action.payload.name,
                cards: action.payload.cards
            };
            return [...state, newDeck];
        default:
            return state;
    }
}

export default reducer;