import { TOWER, TROOPS, SPELL } from "../costants";

const initialState = [
    {
        cardId: 1,
        copies: 7
    },
    {
        cardId: 2,
        copies: 8
    },
    {
        cardId: 3,
        copies: 9
    },
    {
        cardId: 4,
        copies: 4
    },
    {
        cardId: 5,
        copies: 6
    },
    {
        cardId: 6,
        copies: 2
    }
];

function reducer (state = initialState, action){
    return state;
}

export default reducer;