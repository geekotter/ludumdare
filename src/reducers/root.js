import { combineReducers } from "redux";

import collection from './collection';
import decks from './decks';
import game from './game';
import money from './money';


export default combineReducers({
    collection,
    decks,
    game,
    money
});