// Card types
export const TOWER = "TOWER";
export const TROOPS = "TROOPS";
export const SPELL = "SPELL";

// Resources
export const HEALTH = "HEALTH";
export const MANA = "MANA";

// Game phases
export const SPAWNING = "SPAWNING";
export const DRAW = "DRAW";
export const PREPARATION = "PREPARATION";
export const FIGTH = "FIGTH";
export const REGEN = "REGEN";

// Game status
export const ONGOING = "ONGOING";
export const ENDED = "ENDED";