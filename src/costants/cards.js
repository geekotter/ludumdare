import { TOWER, TROOPS, SPELL } from "./index";

export const CARDS = [
    {
        id: 1,
        name:"Archer tower",
        type:TOWER,
        description:"A tower with two archers.",
        atk:5,
        range:3,
    },
    {
        id: 2,
        name:"Infantry",
        type:TROOPS,
        description:"A basic infantry unit.",
        atk:4,
        hp:20
    },
    {
        id: 3,
        name:"Fireball",
        type:SPELL,
        description:"Throw a fireball on your enemies.",
        atk:10,
        manaCost:3
    },
    {
        id: 4,
        name:"Lightning",
        type:SPELL,
        description:"Zap the foes.",
        atk:15,
        manaCost:4
    },
    {
        id: 5,
        name:"Cannon tower",
        type:TOWER,
        description:"A tower with a cannon.",
        atk:10,
        range:1
    },
    {
        id: 6,
        name:"Archers",
        type:TROOPS,
        description:"A unit of specilized archers.",
        atk:8,
        hp:10
    }
];