import React, { Component } from 'react';

import { Link } from 'react-router-dom';

class Navbar extends Component{
    render(){
        return (
            <nav className="navbar navbar-dark bg-dark navbar-expand-md fixed-top">
                <Link to="/" className="navbar-brand">Cards TD</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/play" className="nav-link">Play</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/collection" className="nav-link">Collection</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/decks-manager" className="nav-link">Decks Manager</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/shop" className="nav-link">Shop</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navbar;