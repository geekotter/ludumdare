import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Collection from './pages/collection';
import CreateDeck from './pages/decks-manager/new';
import DecksManager from './pages/decks-manager';
import Home from './pages/home';
import Navbar from './layout/Navbar';
import Play from './pages/play';
import Shop from './pages/shop';

class App extends Component{
    render(){
        return (
            <div className='container'>
                <BrowserRouter>
                    <div>
                        <Navbar />
                        <Route exact path='/' component={Home}/>
                        <Route path='/collection' component={Collection}/>
                        <Route exact path='/decks-manager' component={DecksManager}/>
                        <Route path='/decks-manager/new' component={CreateDeck}/>
                        <Route path='/play' component={Play}/>
                        <Route path='/shop' component={Shop}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;