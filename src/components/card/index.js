import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Card extends Component {
    render() {
        const {id, name, type, description, atk, range, hp, manaCost} = this.props.card;
        const { copies, onDragStart } = this.props;
        let cardClass = "card " + type.toLowerCase();
        return (
            <div className={cardClass} cardid={id} onDragStart={onDragStart} draggable="true">
                <h2>{name}</h2><br/>
                <p>
                    {description}
                </p><br/>
                {atk !== undefined && <h3>Atk: {atk}</h3>}<br/>
                {range !== undefined && <h3>Range: {range}</h3>}
                {hp !== undefined && <h3>Hp: {hp}</h3>}
                {manaCost !== undefined && <h3>Mana cost: {manaCost}</h3>}
                <br/>
                {copies !== undefined && <h3>Copies: {copies}</h3>}
            </div>
        );
    }
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
    copies: PropTypes.number,
    onDragStart: PropTypes.func
};

export default Card;