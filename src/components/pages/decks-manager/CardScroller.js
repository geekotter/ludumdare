import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Card from '../../card';
import { CARDS } from '../../../costants/cards';

class CardScroller extends Component {
    render() {
        const {id, onDragStart, onDragOver, onDrop} = this.props;
        const cards = this.props.cards.map((card) => {
            return <Card 
                        key={card.cardId}
                        card={_.find(CARDS, {'id': card.cardId})}
                        copies={card.copies}
                        onDragStart={onDragStart}
            />
        });
        return (
            <div className="scroll form-group" onDragOver={onDragOver} id={id} onDrop={onDrop}>
                {cards}
            </div>
        );
    }
}

CardScroller.propTypes = {
    id: PropTypes.string.isRequired,
    cards: PropTypes.array.isRequired,
    onDragStart: PropTypes.func.isRequired,
    onDragOver: PropTypes.func,
    onDrop: PropTypes.func
};

export default CardScroller;