import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Deck extends Component {
    render() {
        const { name } = this.props.deck;
        return (
            <div className="deck">
                {name}
            </div>
        );
    }
}

Deck.propTypes = {
    deck: PropTypes.object.isRequired
};

export default Deck;