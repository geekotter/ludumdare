import React, { Component } from 'react';
import { connect } from 'react-redux';
import Proptypes from 'prop-types';
import { Link } from "react-router-dom";

import Deck from './Deck';

class DeckManager extends Component {
    render() {
        const decks = this.props.decks.map((deck) => {
            return <Deck
                        key={deck.id}
                        deck={deck}
            />
        });
        return (
            <div>
                <h1>Decks Manager</h1>
                <div>
                    {decks}
                    <div className="deck">
                        <Link to="/decks-manager/new">Create New</Link>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        decks: state.decks
    };
}

DeckManager.proptypes = {
    decks: Proptypes.array.isRequired
}

export default connect(
    mapStateToProps,
)(DeckManager);