import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Redirect } from "react-router-dom";

import { createDeck } from "../../../../actions/deckActions";

import CardScroller from '../CardScroller';

class CreateDeck extends Component {
    constructor(props){
        super(props);
        this.state = {
            editing: [],
            redirect: false
        };

        this.onDragStart = this.onDragStart.bind(this);
        this.onDragOver = this.onDragOver.bind(this);
        this.onDropCollection = this.onDropCollection.bind(this);
        this.onDropEditing = this.onDropEditing.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    addCard(cardId){
        const newEditing = this.state.editing.slice();
        const index = _.findIndex(newEditing, {cardId: cardId});
        if(index > -1){
            const collectionCard = _.find(this.props.collection, {cardId: cardId});
            if(newEditing[index].copies === collectionCard.copies){
                alert("You don't have any more copies");
            }else{
                newEditing[index].copies += 1;
            }
        }else{
            const newCard = {
                cardId: cardId,
                copies: 1
            };
            newEditing.push(newCard);
        }
        this.setState({
            editing: newEditing
        });
    }

    removeCard(cardId){
        const newEditing = this.state.editing.slice();
        const index = _.findIndex(newEditing, {cardId: cardId});
        if(newEditing[index].copies > 1){
            newEditing[index].copies -= 1;
        }else{
            newEditing.splice(index, 1);
        }
        this.setState({
            editing: newEditing
        });
    }

    onDragStart(event){
        const json = "{\"cardId\": " + event.target.attributes['cardid'].value + ", \"parent\": \"" + event.target.parentNode.id + "\"}";
        event.dataTransfer.setData("text/plain", json);
    }

    onDragOver(event){
        event.preventDefault();
        event.dataTransfer.dropEffect = "copy";
    }

    onDropCollection(event){
        event.preventDefault();
        const data = JSON.parse(event.dataTransfer.getData("text"));
        if(data.parent === "deck"){
            this.removeCard(data.cardId);
        }
    }

    onDropEditing(event){
        event.preventDefault();
        const data = JSON.parse(event.dataTransfer.getData("text"));
        if(data.parent === "collection"){
            this.addCard(data.cardId);
        }
    }

    onSubmit(){
        const name = document.getElementById("name").value;
        const cards = this.state.editing;
        this.props.createDeck(name, cards);
        this.setState({redirect: true});
    }

    render() {
        if(this.state.redirect){
            return <Redirect to="/decks-manager"/>
        }
        return (
            <div>
                <h1>New deck</h1>
                <div>
                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input type="text" id="name" className="form-control" placeholder="Deck name"/>
                    </div>
                    <div className="form-group">
                        <CardScroller
                            id="collection" 
                            cards={this.props.collection}
                            onDragStart={this.onDragStart}
                            onDragOver={this.onDragOver}
                            onDrop={this.onDropCollection}
                        />
                        <CardScroller
                            id="deck" 
                            cards={this.state.editing}
                            onDragStart={this.onDragStart}
                            onDragOver={this.onDragOver}
                            onDrop={this.onDropEditing}
                        />
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary form-control" onClick={this.onSubmit}>Submit</button>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        collection: state.collection
    }
}

CreateDeck.proptypes = {
    collection: PropTypes.array.isRequired
}

export default connect(
    mapStateToProps,
    { createDeck }
)(CreateDeck);