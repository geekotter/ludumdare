import React, { Component } from 'react';
import { connect } from "react-redux";
import Proptypes from "prop-types";
import _ from 'lodash';

import Card from '../../card';
import { CARDS } from '../../../costants/cards';

class Collection extends Component {
    render() {
        const cards = this.props.collection.map((card) => {
            return <Card 
                        key={card.cardId}
                        card={_.find(CARDS, {'id': card.cardId})}
                        copies={card.copies}
            />
        });
        return (
            <div>              
                <h1>Collection</h1>
                <div>
                    {cards}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        collection: state.collection
    }
}

Collection.proptypes = {
    collection: Proptypes.array.isRequired
}

export default connect(mapStateToProps)(Collection);